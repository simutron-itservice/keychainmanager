//
//  KeychainManager.swift
//
//
//  Created by Andreas Geitmann on 07.09.20.
//

import Foundation

enum KeychainError: Error {
    case itemMissing
}

public protocol KeychainManagerProtocol {
    func save(key: String, data: Data) -> OSStatus
    func load(key: String) -> Data?
    func delete(key: String, data: Data) throws -> OSStatus
    func delete(key: String) throws -> OSStatus
    func update(key: String, data: Data)
    
    func loadString(key: String) -> String?
    func saveString(key: String, text: String) -> OSStatus
}

public class KeychainManager: KeychainManagerProtocol {
    public func loadString(key: String) -> String? {
        if let data = load(key: key) {
            return String(decoding: data, as: UTF8.self)
        }
        return nil
    }
    
    public func saveString(key: String, text: String) -> OSStatus {
        if let data = text.data(using: .utf8) {
            return save(key: key, data: data)
        }
        return -1
    }
    
    private let keychain: KeychainProtocol
    
    public init() {
        self.keychain = Keychain()
    }
    
    init(_ keychain: KeychainProtocol = Keychain() ) {
        self.keychain = keychain
    }
    
    public func update(key: String, data: Data) {
        let query: [String: Any] = [kSecAttrAccount as String : key,
                                    kSecValueData as String: data,
                                    kSecMatchLimit as String  : kSecMatchLimitOne]
        
        let attributes: [String: Any] = [kSecAttrAccount as String: key,
                                         kSecValueData as String: data]

        _ = keychain.update(query, with: attributes)
    }

    
    public func delete(key: String) throws -> OSStatus {
        if let data = load(key: key) {
            let query = createSaveQuery(key: key, data: data )
            return keychain.delete(query)
        }
        throw KeychainError.itemMissing
    }
    
    public func delete(key: String, data: Data) throws -> OSStatus {
        let query = createSaveQuery(key: key, data: Data.init() )
        return keychain.delete(query)
    }
    
    public func save(key: String, data: Data) -> OSStatus {
        let query = createSaveQuery(key: key, data: data)
        return keychain.add(query)
    }
    
    public func load(key: String) -> Data? {
        let query = createSearchQuery(key: key)
        return keychain.search(query)
    }
    
    func createSaveQuery(key: String, data: Data) -> [String: Any] {
        // Build the query to be used in Saving Data
        let query = [
            kSecClass as String       : kSecClassGenericPassword as String,
            kSecAttrAccount as String : key,
            kSecValueData as String   : data ]
            as [String : Any]
        return query
    }
    
    func createSearchQuery(key: String) -> [String: Any] {
        // Build the query to be used in Loading Data
        let query = [
            kSecClass as String       : kSecClassGenericPassword,
            kSecAttrAccount as String : key,
            kSecReturnData as String  : kCFBooleanTrue!,
            kSecMatchLimit as String  : kSecMatchLimitOne ]
            as [String : Any]
        return query
    }
}
