//
//  Keychain.swift
//  
//
//  Created by Andreas Geitmann on 07.09.20.
//

import Foundation

public protocol KeychainProtocol {
    func add(_ query: [String: Any]) -> OSStatus
    func search(_ query: [String: Any]) -> Data?
    func update(_ query: [String: Any], with attributes: [String: Any]) -> OSStatus
    func delete(_ query: [String: Any]) -> OSStatus
}


public class Keychain: KeychainProtocol {
    
    /// Update an existing record in the Keychain
    ///
    /// - Parameters:
    ///   - query: A dictionary containing the query
    ///   - attributes: The attributes to update
    /// - Returns: The Security Framework Result Code
    public func update(_ query: [String: Any], with attributes: [String: Any]) -> OSStatus {
        //print (SecItemUpdate(query as CFDictionary, attributes as CFDictionary))
        return SecItemUpdate(query as CFDictionary, attributes as CFDictionary)
    }
    
    /// Fetch data from the KeyChain
    ///
    /// - Parameter query: A dictionary containing the query
    /// - Returns: The data returned as an optional type
    public func search(_ query: [String : Any]) -> Data? {
        var dataTypeRef: AnyObject? = nil
        
        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)
        
        if status == noErr {
            return dataTypeRef as! Data?
        } else {
            return nil
        }
    }
    
    /// Add data to the KeyChain, making sure that the entry does not already exist
    ///
    /// - Parameter query: A dictionary containing the query
    /// - Returns: The Security Framework Result Code
    public func add(_ query: [String : Any]) -> OSStatus {
        _ = delete(query)
            //SecItemDelete(query as CFDictionary)
        return SecItemAdd(query as CFDictionary, nil)
    }
    
    /// Remove data from the Keychain
    ///
    /// - Parameter query: A dictionary containing the query, including data
    /// - Returns: The Security Framework Result Codes
    public func delete(_ query: [String: Any]) -> OSStatus {
        return SecItemDelete(query as CFDictionary)
    }

}
