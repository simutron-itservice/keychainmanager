//
//  KeycahinTests.swift
//  
//
//  Created by Andreas Geitmann on 07.09.20.
//

import XCTest
@testable import KeychainManager

final class KeychainTests: XCTestCase {
    
       func testKC() {
           let manager = KeychainManager()
           if let dta = "testData".data(using: .utf16) {
               let ret = manager.save(key: "test", data: dta )
               XCTAssertEqual(ret, 0)
           }
       }
       
    func testLoadAndSave() {
        let manager = KeychainManager()

        let testKey = "test"
        let testData = "TestData"
        
        // save data to keychain
        if let dta = testData.data(using: .utf8) {
            let ret = manager.save(key: testKey, data: dta)
            XCTAssertEqual(ret, 0)
        }
        
        // load data from keychain
        if let data = manager.load(key: testKey) {
            XCTAssertNotNil(data)
            
            let result = String(decoding: data, as: UTF8.self)
            XCTAssertEqual(result, testData)
        }
    }
    
    func testLoadAndSaveString() {
        let keychain = Keychain()
        let manager = KeychainManager(keychain)

        let testKey = "test"
        let testData = "TestData"
        
        // save data to keychain
        let ret = manager.saveString(key: testKey, text: testData)
        XCTAssertEqual(ret, 0)
        
        // load data from keychain
        let result = manager.loadString(key: testKey)
        XCTAssertEqual(result, testData)
    }

    
    /*
       func testSaveQuery() {
           let mockKeychain = KeychainMock()
           let manager = KeychainManager(mockKeychain)
           guard let dta = "A Test String".data(using: .utf16) else { XCTFail(); return }
           let query = manager.createSaveQuery(key: "Test", data: dta )
           
           let exampleDictionary: [String: Any] = [
               "v_Data": dta,
               "class": "genp",
               "acct": "Test",
           ]
           
           XCTAssertTrue(query == exampleDictionary)
       }
       
    
       func testSaveQueryNoData() {
           let mockKeychain = KeychainMock()
           let manager = KeychainManager(mockKeychain)
           guard let dta = "".data(using: .utf16) else { XCTFail(); return }
           let query = manager.createSaveQuery(key: "Test", data: dta )
           
           let exampleDictionary: [String: Any] = [
               "v_Data": dta,
               "class": "genp",
               "acct": "Test",
           ]
           
           XCTAssertTrue(query == exampleDictionary)
       }

       func testLoadQuery() {
           let mockKeychain = KeychainMock()
           let manager = KeychainManager(mockKeychain)
           let query = manager.createSearchQuery(key: "A Test String")
           let exampleDictionary: [String: Any] = [
               "m_Limit": kSecMatchLimitOne,
               "class": "genp" as CFString,
               "acct": "A Test String",
               "r_Data": kCFBooleanTrue!,
           ]
           XCTAssertTrue(query == exampleDictionary)
       }
       
       func testLoadQueryNoKey() {
           let mockKeychain = KeychainMock()
           let manager = KeychainManager(mockKeychain)
           let query = manager.createSearchQuery(key: "")
           let exampleDictionary: [String: Any] = [
               "m_Limit": kSecMatchLimitOne,
               "class": "genp" as CFString,
               "acct": "",
               "r_Data": kCFBooleanTrue!,
           ]
           XCTAssertTrue(query == exampleDictionary)
       }
*/
    static var allTests = [
        ("testKC", testKC),
    ]
 
}
