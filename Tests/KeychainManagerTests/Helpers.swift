//
//  Helpers.swift
//  
//
//  Created by Andreas Geitmann on 07.09.20.
//

import Foundation

import Foundation

public func ==(lhs: [String: Any], rhs: [String: Any] ) -> Bool {
    return NSDictionary(dictionary: lhs).isEqual(to: rhs)
}
