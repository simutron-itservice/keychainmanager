//
//  File.swift
//  
//
//  Created by Andreas Geitmann on 07.09.20.
//

import Foundation

@testable import KeychainManager

class KeychainMock: KeychainProtocol {
    func update(_ query: [String : Any], with attributes: [String : Any]) -> OSStatus {
        return 0
        
    }
    
    func delete(_ query: [String : Any]) -> OSStatus {
        return 0
    }
    
    func add(_ query: [String : Any]) -> OSStatus {
        return 0
    }
    
    func search(_ query: [String : Any]) -> Data? {
        return nil
    }
    
    
}
