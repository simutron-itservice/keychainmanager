import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(KeychainManagerTests.allTests),
        testCase(KeychainTests.allTests),
    ]
}
#endif
