import XCTest
@testable import KeychainManager

final class KeychainManagerTests: XCTestCase {
    
       func testKC() {
           let mockKeychain = KeychainMock()
           let manager = KeychainManager(mockKeychain)
           if let dta = "testData".data(using: .utf16) {
               let ret = manager.save(key: "test", data: dta )
               XCTAssertEqual(ret, 0)
           }
       }
       
    
       func testSaveQuery() {
           let mockKeychain = KeychainMock()
           let manager = KeychainManager(mockKeychain)
           guard let dta = "A Test String".data(using: .utf16) else { XCTFail(); return }
           let query = manager.createSaveQuery(key: "Test", data: dta )
           
           let exampleDictionary: [String: Any] = [
               "v_Data": dta,
               "class": "genp",
               "acct": "Test",
           ]
           
           XCTAssertTrue(query == exampleDictionary)
       }
       
    
       func testSaveQueryNoData() {
           let mockKeychain = KeychainMock()
           let manager = KeychainManager(mockKeychain)
           guard let dta = "".data(using: .utf16) else { XCTFail(); return }
           let query = manager.createSaveQuery(key: "Test", data: dta )
           
           let exampleDictionary: [String: Any] = [
               "v_Data": dta,
               "class": "genp",
               "acct": "Test",
           ]
           
           XCTAssertTrue(query == exampleDictionary)
       }

       func testLoadQuery() {
           let mockKeychain = KeychainMock()
           let manager = KeychainManager(mockKeychain)
           let query = manager.createSearchQuery(key: "A Test String")
           let exampleDictionary: [String: Any] = [
               "m_Limit": kSecMatchLimitOne,
               "class": "genp" as CFString,
               "acct": "A Test String",
               "r_Data": kCFBooleanTrue!,
           ]
           XCTAssertTrue(query == exampleDictionary)
       }
       
       func testLoadQueryNoKey() {
           let mockKeychain = KeychainMock()
           let manager = KeychainManager(mockKeychain)
           let query = manager.createSearchQuery(key: "")
           let exampleDictionary: [String: Any] = [
               "m_Limit": kSecMatchLimitOne,
               "class": "genp" as CFString,
               "acct": "",
               "r_Data": kCFBooleanTrue!,
           ]
           XCTAssertTrue(query == exampleDictionary)
       }

    static var allTests = [
        ("testKC", testKC),
        ("testSaveQuery", testSaveQuery),
    ]
 
}
